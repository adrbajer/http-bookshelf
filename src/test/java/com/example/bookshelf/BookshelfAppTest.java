package com.example.bookshelf;

import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

class BookshelfAppTest {

    private static final String BOOK_1 = "{\n" +
            "\"id\": 333,\n" +
            "\"title\": \"Harry Pontus\",\n" +
            "\"author\": \"J.K\",\n" +
            "\"pagesSum\": 500,\n" +
            "\"yearOfPublished\": 2002,\n" +
            "\"publishingHouse\": \"publishing company ###\"\n" +
            "}";
    private static final String BOOK_2 = "{\n" +
            "  \"id\": 333,\n" +
            "  \"title\": \"Harry Pontus\",\n" +
            "  \"author\": \"J.K\",\n" +
            "  \"pagesSum\": \"1000 pages\",\n" +
            "  \"yearOfPublished\": 2002,\n" +
            "  \"publishingHouse\": \"publishing company ###\"\n" +
            "}";

    private static final int APP_PORT = 8083;

    private BookshelfApp bookshelfApp;

    @BeforeAll
    public static void beforeAll() {
        RestAssured.port = APP_PORT;
    }

    @BeforeEach
    public void beforeEach() throws Exception {
        bookshelfApp = new BookshelfApp(APP_PORT);
    }

    @AfterEach
    public void afterEach() {
        bookshelfApp.stop();
    }

    @Test
    public void addMethod_correctBody_shouldReturnStatus200() throws Exception {
        with().body(BOOK_1).when().post("/book/add").then().statusCode(200).body(startsWith("Book has been successfully added, id="));
//        with().body(BOOK_1).when().post("/book/add").then().statusCode(200).body(equalTo("success"));
    }

    @Test
    public void addMethod_fieldTypeMismatch_shouldReturnStatus500() {
        with().body(BOOK_2).when().post("/book/add").then().statusCode(500);
    }

    @Test
    public void addMethod_unexpectedField_shouldReturnStatus500() {
        with().body("{\"numberOfChapters\":10}").when().post("/book/add").then().statusCode(500);
    }

}