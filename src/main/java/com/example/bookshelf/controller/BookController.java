package com.example.bookshelf.controller;

import com.example.bookshelf.storage.BookStorage;
import com.example.bookshelf.storage.impl.StaticListBookStorageImpl;
import com.example.bookshelf.type.Book;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.iki.elonen.NanoHTTPD;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static fi.iki.elonen.NanoHTTPD.Response.Status.*;
import static fi.iki.elonen.NanoHTTPD.newFixedLengthResponse;

public class BookController {

    private final static String BOOK_ID_PARAM_NAME = "bookId";

    private BookStorage bookStorage = new StaticListBookStorageImpl();



    public NanoHTTPD.Response serveRemoveBookRequest (NanoHTTPD.IHTTPSession session){

        Map<String, List<String>> requestParameters = session.getParameters();
        if (requestParameters.containsKey(BOOK_ID_PARAM_NAME)) {
            List<String> bookIdParams = requestParameters.get(BOOK_ID_PARAM_NAME);
            String bookIdParam = bookIdParams.get(0);
            long bookId = 0;

            try {
                bookId = Long.parseLong(bookIdParam);
            } catch (NumberFormatException nfe) {
                System.err.println("Error during convert request param: \n" + nfe);
                return newFixedLengthResponse(BAD_REQUEST, "text/plain", "Request param 'bookId' has to be a number");
            }

            List<Long> integerList = new ArrayList<Long>();
            bookStorage.getAllBooks().forEach( x -> integerList.add( x.getId() ) );

            if (integerList.contains(bookId)){
                bookStorage.removeBook(bookId);

                return  newFixedLengthResponse(OK,"text/plain","Book with id " + bookId + " has been removed");
            }
            return newFixedLengthResponse(NOT_FOUND, "application/json", "");

        }
        return newFixedLengthResponse(BAD_REQUEST, "text/plain", "Incorrect request params");
    }



    public NanoHTTPD.Response serveGetBookRequest (NanoHTTPD.IHTTPSession session) {

        Map<String, List<String>> requestParameters = session.getParameters();
        if (requestParameters.containsKey(BOOK_ID_PARAM_NAME)) {
            List<String> bookIdParams = requestParameters.get(BOOK_ID_PARAM_NAME);
            String bookIdParam = bookIdParams.get(0);
            long bookId = 0;

            try {
                bookId = Long.parseLong(bookIdParam);
            } catch (NumberFormatException nfe) {
                System.err.println("Error during convert request param: \n" + nfe);
                return newFixedLengthResponse(BAD_REQUEST, "text/plain", "Request param 'bookId' has to be a number");
            }

            Book book = bookStorage.getBook(bookId);
            if (book != null) {
                try{
                    ObjectMapper objectMapper = new ObjectMapper();
                    String response = objectMapper.writeValueAsString(book);
                    return newFixedLengthResponse(OK, "application/json", response);

                } catch (JsonProcessingException e) {
                    System.err.println("Error during process request: \n" + e);
                    return newFixedLengthResponse(INTERNAL_ERROR, "text/plain", "Internal error can't read selected book");
                }
            }
            return newFixedLengthResponse(NOT_FOUND,"application/json","");
        }
        return newFixedLengthResponse(BAD_REQUEST, "text/plain", "Incorrect request params");
    }



    public NanoHTTPD.Response serveGetBooksRequest (NanoHTTPD.IHTTPSession session) {

        ObjectMapper objectMapper = new ObjectMapper();
        String response = "";

        try {
            response = objectMapper.writeValueAsString(bookStorage.getAllBooks());
        } catch (JsonProcessingException e) {
            System.err.println("Error during process request: \n" + e);
            return NanoHTTPD.newFixedLengthResponse(INTERNAL_ERROR, "text/plain", "Internal error can't read all book.");
        }
        return NanoHTTPD.newFixedLengthResponse(OK, "application/json", response);
    }



    public NanoHTTPD.Response serveAddBookRequest (NanoHTTPD.IHTTPSession session) {


        ObjectMapper objectMapper = new ObjectMapper();
//        long randomBookId = System.currentTimeMillis();
        String lengthHeader = session.getHeaders().get("content-length");
        int contentLength = Integer.parseInt(lengthHeader);
        byte[] buffer = new byte[contentLength];
        Book requestBook;
        try {
            session.getInputStream().read(buffer, 0, contentLength);
            String requestBody = new String(buffer).trim();
            requestBook = objectMapper.readValue(requestBody, Book.class);
//            requestBook.setId(randomBookId);
            List<Long> integerList = new ArrayList<Long>();
            bookStorage.getAllBooks().forEach( x -> integerList.add( x.getId() ) );
            if(integerList.contains( requestBook.getId() )){
                return newFixedLengthResponse(BAD_REQUEST, "text/plain", "Book with selected id already exists");
            }
            bookStorage.addBook(requestBook);
        } catch (IOException e) {
            System.err.println("Error during process request: \n" + e);
            return newFixedLengthResponse(INTERNAL_ERROR, "text/plain", "Internal error book hasn't been added");
        }
//        return newFixedLengthResponse(OK, "text/plain", "Book has been successfully added, id=" + randomBookId);
        return newFixedLengthResponse(OK, "text/plain", "Book has been successfully added, id=" + requestBook.getId());
    }
}
